package lemry.thedrake;

import java.util.LinkedList;
import java.util.List;

public class TroopTile implements Tile{
    private final Troop troop;
    private final PlayingSide side;
    private final TroopFace face;

    public TroopTile(Troop troop, PlayingSide side, TroopFace face) {
        this.troop = troop;
        this.side = side;
        this.face = face;
    }

    public TroopFace face() {
        return face;
    }
    public PlayingSide side() { return side; }
    public Troop troop() {
        return troop;
    }

    public boolean canStepOn() {
        return false;
    }

    public boolean hasTroop() {
        return true;
    }

    @Override
    public List<Move> movesFrom(BoardPos pos, GameState state) {
            List<Move>moves=new LinkedList<Move>();
        for(TroopAction a :troop.actions(face))
            moves.addAll(a.movesFrom(pos,side,state));
        return moves;
    }

    public TroopTile flipped() {
        return face() == TroopFace.AVERS ? new TroopTile(troop, side, TroopFace.REVERS) : new TroopTile(troop, side, TroopFace.AVERS);
    }
}
