package lemry.thedrake;

import java.util.List;

public class Board {
    private final int dimension;
    private BoardTile[][] board;

    public Board(int dimension) {
        this.dimension = dimension;
        board = new BoardTile[dimension][dimension];

        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) board[i][j] = BoardTile.EMPTY;
        }
    }

    public Board(Board from) {
        this.dimension = from.dimension;
        board = new BoardTile[dimension][dimension];

        for (int i = 0; i < dimension; i++) {
            board[i] = from.board[i].clone();
        }
    }

    public int dimension() {
        return dimension;
    }

    public BoardTile at(BoardPos pos) {
        return board[pos.i()][pos.j()];
    }

    public Board withTiles(TileAt... ats) {
        Board newBoard = new Board(this);

        for (int i = 0; i < ats.length; i++) {
            newBoard.board[ats[i].pos.i()][ats[i].pos.j()] = ats[i].tile;
        }
        return newBoard;
    }


    public static class TileAt {
        public final BoardPos pos;
        public final BoardTile tile;

        public TileAt(BoardPos pos, BoardTile tile) {
            this.pos = pos;
            this.tile = tile;
        }
    }

    public PositionFactory positionFactory() {
        return new PositionFactory(dimension);
    }
}


